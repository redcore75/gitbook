# About

## **RCS Biz Center Brand Feed API Version 1.0.0**

본 문서는 기업과 대행사에 제공되는 RCS Biz Center 브랜드 소식 API의 규격을 기술합니다.

브랜드 소식 API의 사용을 위해서는 사전에 RCS Biz Center에 기업/대행사 등록 및 브랜드 등록이 필요합니다.

추가적인 보안을 위해 API를 연동하는 Client의 IP 를 RCS Biz Center에 사전 등록하여야 합니다. RCS Biz Center 홈페이지 "내 정보관리"에서 등록하실 수 있습니다.

API를 연동하고자 하는 경우 검수(STG) 환경에 가입 및 승인 후 사전 개발 진행할 수 있도록 제공하고 있습니다.

1. 검수(STG) - https://api-qa.rcsbizcenter.com/bfapi/1.0
2. 상용(PRD) - https://api.rcsbizcenter.com/bfapi/1.0

RCS Biz Center 에서 제공하는 API는 이통 3사가 정의한 정책에 의거하여 정보를 제공합니다. 따라서 일부 정보 제공 상의 제약이 있을 수 있습니다.

**API 문의처 : 1522-5739 / tech@rcsbizcenter.com**

`수정 이력`

>

**2021년 11월 25일 RCS Biz Center 브랜드 소식 API 1.0 배포**
