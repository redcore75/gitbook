# Feed

{% swagger src="../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml" path="/brand/{brandId}/feed" method="get" %}
[MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml](<../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml" path="/brand/{brandId}/feed" method="post" %}
[MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml](<../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml" path="/brand/{brandId}/feed/{feedId}" method="get" %}
[MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml](<../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml" path="/brand/{brandId}/feed/{feedId}" method="put" %}
[MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml](<../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml>)
{% endswagger %}

{% swagger src="../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml" path="/brand/{brandId}/feed/{feedId}" method="delete" %}
[MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml](<../../../.gitbook/assets/MaaP_KR RCS_Biz_Center_Brand_Feed 1.0.0 resolved.yaml>)
{% endswagger %}
