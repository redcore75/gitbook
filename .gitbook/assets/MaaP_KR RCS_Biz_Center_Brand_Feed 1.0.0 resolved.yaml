---
openapi: 3.0.0
info:
  title: RCS Biz Center 브랜드 소식 API 규격
  description: "# **RCS Biz Center Brand Feed API Version 1.0.0** \n\n본 문서는 기업과 대행사에 제공되는 RCS Biz Center 브랜드 소식 API의 규격을 기술합니다.\n\n브랜드 소식 API의 사용을 위해서는 사전에 RCS Biz Center에 기업/대행사 등록 및 브랜드 등록이 필요합니다.\n\n추가적인 보안을 위해 API를 연동하는 Client의 IP 를 RCS Biz Center에 사전 등록하여야 합니다.  RCS Biz Center 홈페이지 \\\"내 정보관리\\\"에서 등록하실 수 있습니다.\n\nAPI를 연동하고자 하는 경우 검수(STG) 환경에 가입 및 승인 후 사전 개발 진행할 수 있도록 제공하고 있습니다.\n\n\n1. 검수(STG) - https://api-qa.rcsbizcenter.com/bfapi/1.0\n2. 상용(PRD) - https://api.rcsbizcenter.com/bfapi/1.0\n\n\nRCS Biz Center 에서 제공하는 API는 이통 3사가 정의한 정책에 의거하여 정보를 제공합니다. 따라서 일부 정보 제공 상의 제약이 있을 수 있습니다.\n\n**API 문의처 : 1522-5739 / tech@rcsbizcenter.com**\n\n\n`수정 이력`\n>\n**2021년 11월 25일 RCS Biz Center 브랜드 소식 API 1.0 배포**\n"
  version: 1.0.0
externalDocs:
  description: "※ 브랜드 소식 API 연동을 위해서는 인증 token과 브랜드 ID(대행사는 추가로 해당 브랜드의 권한 확인용 브랜드 Key도 요구됨)가 필요하며,\n이를 위해 RCS Biz Center Open API 규격서 명시된 API를 이용하여야 합니다. \n자세한 사항은 링크된 규격서를 참고해 주시기 바랍니다.\n"
  url: https://app.swaggerhub.com/apis-docs/MaaP_KR/RCS_Biz_Center/1.1.5
servers:
- url: https://api-qa.rcsbizcenter.com/bfapi/1.0
  description: RCS Biz Center API for Staging
- url: https://api.rcsbizcenter.com/bfapi/1.0
  description: RCS Biz Center API for Production
security:
- jwtAuth: []
paths:
  /brand/{brandId}/feed:
    get:
      summary: |
        브랜드 소식의 소식 목록을 조회합니다.
      description: |
        브랜드 소식의 소식 목록을 조회합니다.

        브랜드 내 소식은 최대 100개까지 유지됩니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: offset
        in: query
        description: "시작 offset 번호(default: 0) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: limit
        in: query
        description: "조회 최대 건수(default: 100, maximum: 1000) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      responses:
        "200":
          description: ""
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
    post:
      summary: |
        브랜드 소식을 등록합니다.
      description: |
        브랜드 소식을 등록합니다.

        브랜드 소식에 등록 가능한 소식 유형에는 Gallery, Share 2가지 유형이 있습니다. 추후 Slide 유형을 추가로 제공할 예정입니다.

        Gallery 유형 소식 등록 시 포함되는 이미지는 파일 업로드 API를 이용하여 사전 등록해야 합니다.

        이미지 파일 등록 시 발급되는 ID를 이용하여야 소식에 이미지를 표시할 수 있습니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RegBrandFeed'
      responses:
        "200":
          description: 성공적으로 등록된 경우 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_1'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
  /brand/{brandId}/feed/{feedId}:
    get:
      summary: |
        브랜드 소식의 소식 상세 내용을 조회합니다.
      description: |
        브랜드 소식의 소식 상세 내용을 조회합니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: feedId
        in: path
        description: |
          브랜드 소식 내 개별 소식 ID 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: 소식 상세 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_2'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
    put:
      summary: |
        브랜드 소식의 소식 상세 내용을 수정합니다.
      description: |
        브랜드 소식의 소식 상세 내용을 수정합니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: feedId
        in: path
        description: |
          브랜드 소식 내 개별 소식 ID 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RegBrandFeed'
      responses:
        "200":
          description: 소식 상세 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_2'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
    delete:
      summary: |
        브랜드 소식의 소식을 삭제합니다.
      description: |
        브랜드 소식의 소식을 삭제합니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: feedId
        in: path
        description: |
          브랜드 소식 내 개별 소식 ID 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: 소식ID가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_3'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
  /brand/{brandId}/feed/file:
    post:
      summary: |
        소식에 사용되는 이미지 파일을 등록합니다.
      description: |
        소식에 사용되는 이미지 파일을 등록합니다. slide인 경우 정사각형 비율만 허용됩니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      requestBody:
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/feed_file_body'
      responses:
        "200":
          description: 파일 등록 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_4'
  /brand/{brandId}/feed/file/{fileId}:
    get:
      summary: |
        등록된 이미지 파일 정보를 조회합니다.
      description: |
        등록된 이미지 파일 정보를 조회합니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: fileId
        in: path
        description: |
          브랜드 소식에 등록한 파일 ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      responses:
        "200":
          description: 파일 등록 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_4'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
  /brand/{brandId}/feed/stat/main:
    get:
      summary: |
        지정된 브랜드의 소식 메인 통계를 조회 합니다.
      description: |
        지정된 브랜드의 소식 메인 통계를 조회 합니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: offset
        in: query
        description: "시작 offset 번호(default: 0) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: limit
        in: query
        description: "조회 최대 건수(default: 10, maximum: 100) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: startDate
        in: query
        description: |
          검색 시작일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 부터  30일 이전 일자 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
        required: false
        style: form
        explode: true
        schema:
          maximum: 8
          type: string
      - name: endDate
        in: query
        description: |
          검색 종료일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
        required: false
        style: form
        explode: true
        schema:
          maximum: 8
          type: string
      responses:
        "200":
          description: 소식 메인에 대한 통계 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_5'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
  /brand/{brandId}/feed/stat/feed:
    get:
      summary: |
        지정된 브랜드의 소식 상세 통계를 조회 합니다.
      description: |
        지정된 브랜드의 소식 상세 통계를 조회 합니다.
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: offset
        in: query
        description: "시작 offset 번호(default: 0) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: limit
        in: query
        description: "조회 최대 건수(default: 10, maximum: 100) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: type
        in: query
        description: 소식 유형을 지정합니다.
        required: false
        style: form
        explode: true
        schema:
          type: string
          enum:
          - gallery
          - share
          - slide
      - name: startDate
        in: query
        description: |
          검색 시작일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 부터  30일 이전 일자 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
        required: false
        style: form
        explode: true
        schema:
          maximum: 8
          type: string
      - name: endDate
        in: query
        description: |
          검색 종료일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
        required: false
        style: form
        explode: true
        schema:
          maximum: 8
          type: string
      responses:
        "200":
          description: 소식 상세에 대한 통계 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_6'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
  /brand/{brandId}/feed/stat/slide:
    get:
      summary: |
        지정된 브랜드의 슬라이드 소식 상세 통계를 조회 합니다.
      description: "지정된 브랜드의 슬라이드 소식 상세 통계를 조회 합니다.\nSlide유형은 gallery, share 유형으로 구성되므로 type query parameter 지정 조회 시 참고하시기 바랍니다. \n"
      parameters:
      - name: X-RCS-Brandkey
        in: header
        description: |
          RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
          대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
        required: false
        style: simple
        explode: false
        schema:
          maximum: 36
          type: string
      - name: brandId
        in: path
        description: |
          브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
        required: true
        style: simple
        explode: false
        schema:
          type: string
      - name: offset
        in: query
        description: "시작 offset 번호(default: 0) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: limit
        in: query
        description: "조회 최대 건수(default: 10, maximum: 100) 입니다."
        required: false
        style: form
        explode: true
        schema:
          type: integer
      - name: type
        in: query
        description: 소식 유형을 지정합니다.
        required: false
        style: form
        explode: true
        schema:
          type: string
          enum:
          - gallery
          - share
          - slide
      - name: startDate
        in: query
        description: |
          검색 시작일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 부터  30일 이전 일자 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
        required: false
        style: form
        explode: true
        schema:
          maximum: 8
          type: string
      - name: endDate
        in: query
        description: |
          검색 종료일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
        required: false
        style: form
        explode: true
        schema:
          maximum: 8
          type: string
      responses:
        "200":
          description: 슬라이드 소식 상세에 대한 통계 정보가 응답됩니다.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200_7'
        default:
          description: 처리 실패
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_default'
components:
  schemas:
    "400ErrorInfo":
      type: object
      properties:
        status:
          type: string
          example: "400"
        error:
          $ref: '#/components/schemas/400ErrorInfo_error'
      description: |
        요청 정보 상의 오류가 있는 경우 응답됩니다.
        - 64001 "Missing X-RCS-BrandKey header"
        - 64002 "Invalid Brand Key"
        - 64201 "Invalid query parameter (%s)"
        - 64202 "Invalid query parameter value %s"
        - 64203 "query paramter required [%s]"
        - 64301 "Over specified size (%s)"
        - 64302 "Invalid JSON format"
        - 64304 "Over specified size (%s)"
        - 64322 "File does not exist"
        - 64336 "required value: [%s]"
        - 64339 "value is too long: [%s], maxsize is %s"
        - 64354 "Attached file capacity overflow"
        - 64701 "parameter required [%s]"
        - 64702 "parameter invalid [%s]"
        - 64703 "parameter size invalid [%s]"
        - 64704 "Not Brand Approved"
        - 64705 "paramter date over [%s]"
        - 64706 "date compare over [start date: %s], [end date: %s]"
        - 64707 "File extension not available"
        - 64708 "File size not allowed"
        - 64709 "can't delete[pinYn:Y]"
        - 64710 "can't hide[pinYn:Y]"
        - 64711 "Not found FEED"
        - 64712 "value is too long [%s]"
        - 64713 "No File Permission [%s]"
    "401ErrorInfo":
      type: object
      properties:
        status:
          type: string
          example: "401"
        error:
          $ref: '#/components/schemas/401ErrorInfo_error'
      description: "인증에 필요한 정보가 누락되거나 값이 잘못된 경우 응답됩니다.\n- 61001  Missing Authorization header Authorization \n- 61002  Missing Token  Authorization \n- 61003  Invalid token  \n- 61004  Token has expired  \n- 61005  Invalid client id  \n- 61006  Invalid secret key  \n"
    Pagination:
      type: object
      properties:
        total:
          type: integer
          description: 전체 건수
        offset:
          minimum: 0
          type: integer
          description: 조회 기준 위치(기준 ROW)
          default: 0
        limit:
          maximum: 1000
          minimum: 1
          type: integer
          description: 페이지 당 조회 건수
          default: 100
      description: 전체 건수와 페이징 조건을 제공합니다.
    Links:
      type: object
      properties:
        prev:
          type: string
          description: 이전 페이지 URL
        next:
          type: string
          description: 다음 페이지 URL
      description: |
        이전/다음 페이지 링크를 제공합니다. offset을 0이 아닌 limit 보다 작은 양수값으로 설정한 경우 prev 는 null로 응답됩니다.
    RegBrandFeed:
      required:
      - description
      - pinYn
      - publishType
      - status
      - title
      - type
      type: object
      properties:
        title:
          maximum: 40
          type: string
          description: 브랜드 소식 제목
        description:
          maximum: 2000
          type: string
          description: 브랜드 소식 내용
        type:
          type: string
          description: 브랜드 소식 카드 유형
          enum:
          - gallery
          - share
        share:
          $ref: '#/components/schemas/ShareType'
        media:
          $ref: '#/components/schemas/MediaType'
        buttons:
          $ref: '#/components/schemas/Buttons'
        status:
          type: string
          description: |
            등록 시 지정할 소식의 상태값

            - save: 저장(최종 등록되지 않은 임시저장 상태)
            - ok: 등록
            - hide: 비공개
          enum:
          - save
          - ok
          - hide
        publishType:
          type: string
          description: |
            소식의 게시 유형

            - publish: 즉시게시
            - reservation: 예약 - 지정한 일자에 소식목록에 표시되도록 예약된 소식
            - internet: 숨김게시 - 소식목록에 표시되지 않지만 URL을 통해 접근 가능한 소식
          enum:
          - publish
          - reservation
          - internet
        publishDate:
          type: string
          description: |
            게시 일자. publishType이 reservation 인 경우 반드시 지정(형식 - YYYYMMDDhhmmss)
          example: "201210181411"
        pinYn:
          type: string
          description: 소식 메인 상단 고정여부. 이미 고정된 소식 존재하는 경우 신규 지정 건으로 대체됨
          default: "N"
      description: |
        브랜드 소식 등록 정보 객체입니다.
        브랜드 소식 등록 시 gallery, share 유형 또는 두 가지 모두 사용 가능한 slide 유형 중 하나을 지정할 수 있습니다.
        각각의 유형은 buttons를 이용하여 소식 내 버튼을 지정할 수 있습니다.
      example:
        title: gallery feed title
        description: gallery feed discription......................
        type: gallery
        media:
        - orderNo: 1
          fileId: string
        - orderNo: 2
          fileId: string
        buttons:
        - type: call
          orderNo: 1
          name: 전화걸기
          link: "15884741"
        - type: app
          orderNo: 2
          name: 앱 다운로드
          appLink:
            packageName: com.android.mobile.cs
            action: android.intent.action.VIEW
            uri: abc://gizmos/foo/bar
        status: status
        publishType: publish
        pinYn: "N"
    BrandFeed:
      required:
      - brandId
      - description
      - feedId
      - pinYn
      - publishType
      - status
      - title
      type: object
      properties:
        brandId:
          maximum: 13
          type: string
          description: 브랜드 ID
        feedId:
          maximum: 10
          type: string
          description: 소식 ID
        title:
          maximum: 40
          type: string
          description: 브랜드 소식 제목
        description:
          maximum: 2000
          type: string
          description: 브랜드 소식 내용
        type:
          type: string
          description: 브랜드 소식 카드 유형
          enum:
          - gallery
          - share
        status:
          type: string
          description: |
            소식의 상태값

            - save: 저장(최종 등록되지 않은 임시저장 상태)
            - ok: 등록
            - hide: 비공개 - 숨김처리되어 목록에 비노출
            - delete: 논리 삭제(삭제된 소식은 수정 불가)
          enum:
          - save
          - ok
          - hide
          - delete
        publishType:
          type: string
          description: |
            소식의 게시 유형

            - publish: 게시 - 정상 게시됨
            - reservation: 예약 - 지정한 일자에 소식목록에 표시되도록 예약된 소식
            - internet: 숨김게시 - 소식목록에 표시되지 않지만 URL을 통해 접근 가능한 소식
          enum:
          - publish
          - reservation
          - internet
        publishDate:
          type: string
          description: 게시 일자.
        pinYn:
          type: string
          description: 소식 메인 상단 고정여부.
          default: "N"
        registerDate:
          type: string
          description: 등록 일시
          format: date-time
        updateDate:
          type: string
          description: 수정 일시
          format: date-time
        registerId:
          type: string
          description: 등록 계정 ID
        updateId:
          type: string
          description: 수정 계정 ID
      description: 브랜드 소식 목록 정보 객체 입니다.
    BrandFeedDetail:
      type: object
      properties:
        share:
          $ref: '#/components/schemas/ShareType'
        media:
          $ref: '#/components/schemas/MediaType'
        buttons:
          $ref: '#/components/schemas/Buttons'
        feedUrl:
          type: string
          description: status가 save가 아닌 경우 소식 URL 제공
      description: 브랜드 소식 상세 정보 객체 입니다.
      allOf:
      - $ref: '#/components/schemas/BrandFeed'
    ShareType:
      required:
      - snsLink
      - title
      type: object
      properties:
        snsLink:
          type: string
          description: "소식 유형이 [share] 타입일때 의 SNS URL"
        title:
          maximum: 80
          type: string
          description: snsLink에서 추출한 open graph tag의 title 값
        description:
          maximum: 2000
          type: string
          description: snsLink에서 추출한 open graph tag의 description 값
        imageUrl:
          type: string
          description: snsLink에서 추출한 open graph tag의 image 값
      description: Share 유형은 SNS 제공처에서 제공하는 오픈그래프 meta 정보를 추출하여 등록해야 합니다. share 유형의 객체입니다.
    MediaType:
      type: array
      description: gallery 유형을 구성하는 이미지 정보 객체입니다.
      items:
        $ref: '#/components/schemas/MediaType_inner'
    AppLink:
      required:
      - action
      - packageName
      - uri
      type: object
      properties:
        packageName:
          maximum: 40
          type: string
          description: App 실행을 위한 package name
          example: com.android.mobile.cs
        scheme:
          maximum: 40
          type: string
          description: App 실행을 위한 scheme
          example: rcsbizapp
        uri:
          maximum: 40
          type: string
          description: App 실행을 위한 URI
          example: abc://gizmos/foo/bar
      description: APP 버튼 정보 객체입니다.
    Buttons:
      type: array
      description: 소식 카드에 지정할 수 있는 버튼 정보 객체입니다.
      items:
        $ref: '#/components/schemas/Buttons_inner'
    SlideType:
      type: array
      description: |
        slide 유형은 gallery, share 유형의 소식을 조합하여 사용합니다.
        slide 유형 내 gallery, share 유형은 각각 buttons을 설정해야 합니다.
      items:
        $ref: '#/components/schemas/SlideType_inner'
    FeedFileInfo:
      type: object
      properties:
        fileId:
          maximum: 64
          type: string
          description: 등록된 파일의 ID 입니다.
        url:
          maximum: 128
          type: string
          description: 등록된 파일의 URL 정보 입니다.
        fileName:
          maximum: 256
          type: string
          description: 등록한 파일의 이름입니다.
        type:
          type: string
          description: 이미지가 사용될 브랜드 소식 카드 유형
          enum:
          - gallery
          - slide
      description: 브랜드 소식 파일 등록 정보입니다.
    FeedMainStat:
      type: object
      properties:
        brandId:
          maximum: 13
          type: string
          description: 브랜드 ID
        ymd:
          maximum: 8
          type: string
          description: 통계일자
        stayTime:
          type: string
          description: 체류시간 단위 초(sec)
        pvCnt:
          type: integer
          description: 당일 페이지 뷰수
        pvTotCnt:
          type: integer
          description: 누적 페이지 뷰수
        clCnt:
          type: integer
          description: 당일 소식 클릭수
        clTotCnt:
          type: integer
          description: 누적 소식 클릭수
      description: 브랜드 소식 메인 통계 정보입니다.
    FeedDetailStat:
      type: object
      properties:
        brandId:
          maximum: 13
          type: string
          description: 브랜드 ID
        feedId:
          maximum: 10
          type: string
          description: 소식 ID
        ymd:
          maximum: 8
          type: string
          description: 통계일자
        type:
          type: string
          description: 소식 유형
          enum:
          - gallery
          - share
          - slide
        pvCnt:
          type: integer
          description: 당일 페이지 뷰수
        pvTotCnt:
          type: integer
          description: 누적 페이지 뷰수
        btn1ClCnt:
          type: integer
          description: 당일 버튼1 클릭수
        btn1ClTotCnt:
          type: integer
          description: 누적 버튼1 클릭수
        btn2ClCnt:
          type: integer
          description: 당일 버튼2 클릭수
        btn2ClTotCnt:
          type: integer
          description: 누적 버튼2 클릭수
        slClCnt:
          type: integer
          description: 당일 슬라이드 클릭수
        slClTotCnt:
          type: integer
          description: 누적 슬라이드 클릭수
      description: 브랜드 소식 상세 통계 정보 입니다.
    FeedSlideStat:
      properties:
        brandId:
          maximum: 13
          type: string
          description: 브랜드 ID
        feedId:
          maximum: 10
          type: string
          description: 소식 ID
        ymd:
          maximum: 8
          type: string
          description: 통계일자
        title:
          type: string
          description: 소식 제목
        orderNo:
          type: integer
          description: 슬라이드 정렬 순번
        slideType:
          type: string
          description: 슬라이드 소식 내 유형
          enum:
          - gallery
          - share
        btn1ClCnt:
          type: integer
          description: 당일 버튼1 클릭수
        btn1ClTotCnt:
          type: integer
          description: 누적 버튼1 클릭수
        btn2ClCnt:
          type: integer
          description: 당일 버튼2 클릭수
        btn2ClTotCnt:
          type: integer
          description: 누적 버튼2 클릭수
      description: 브랜드 소식의 슬라이드 상세 통계 정보 입니다.
    inline_response_200:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/BrandFeed'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
        pagination:
          $ref: '#/components/schemas/Pagination'
        links:
          $ref: '#/components/schemas/Links'
    inline_response_default:
      oneOf:
      - $ref: '#/components/schemas/401ErrorInfo'
      - $ref: '#/components/schemas/400ErrorInfo'
    inline_response_200_1:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/inline_response_200_1_result'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
    inline_response_200_2:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/BrandFeedDetail'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
    inline_response_200_3:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/inline_response_200_3_result'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
    feed_file_body:
      properties:
        file:
          type: string
          description: |
            브랜드 소식에 사용할 이미지 파일입니다.
          format: binary
        type:
          type: string
          description: 이미지가 사용될 브랜드 소식 카드 유형
          enum:
          - gallery
          - slide
    inline_response_200_4:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/FeedFileInfo'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
    inline_response_200_5:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/FeedMainStat'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
        pagination:
          $ref: '#/components/schemas/Pagination'
        links:
          $ref: '#/components/schemas/Links'
    inline_response_200_6:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/FeedDetailStat'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
        pagination:
          $ref: '#/components/schemas/Pagination'
        links:
          $ref: '#/components/schemas/Links'
    inline_response_200_7:
      type: object
      properties:
        result:
          type: array
          items:
            $ref: '#/components/schemas/FeedSlideStat'
        code:
          type: string
          default: "20000000"
        status:
          type: integer
          default: 200
        desc:
          type: string
        pagination:
          $ref: '#/components/schemas/Pagination'
        links:
          $ref: '#/components/schemas/Links'
    "400ErrorInfo_error":
      type: object
      properties:
        code:
          type: string
          example: "64304"
        message:
          type: string
          example: File does not exist
    "401ErrorInfo_error":
      type: object
      properties:
        code:
          type: string
          example: "61001"
        message:
          type: string
          example: Missing Authorization header
    MediaType_inner:
      required:
      - fileId
      - orderNo
      - url
      properties:
        orderNo:
          type: integer
          description: 정렬 순서
        fileId:
          type: string
          description: 이미지 파일 ID
        url:
          type: string
          description: 이미지 URL(파일 등록 결과로 제공됨)
    Buttons_inner:
      required:
      - orderNo
      - title
      - type
      properties:
        type:
          type: string
          description: 버튼 유형
          enum:
          - app
          - url
          - call
          - chat
        orderNo:
          type: integer
          description: 정렬 순서
        title:
          type: string
          description: 버튼명
        applink:
          $ref: '#/components/schemas/AppLink'
        link:
          type: string
          description: |
            버튼 유형에 따른 입력 데이터 형식은 다음과 같습니다.

            - url 인 경우, web URL(http:// or https://로 시작하는 문자열)
            - call 인 경우, 전화번호(숫자만 허용)
            - chat 인 경우, 대화방ID
    SlideType_inner:
      required:
      - description
      - orderNo
      - title
      - type
      properties:
        orderNo:
          type: integer
          description: 정렬 순서
        title:
          maximum: 34
          type: string
          description: 브랜드 소식 제목
        description:
          maximum: 126
          type: string
          description: 브랜드 소식 내용
        type:
          type: string
          description: 브랜드 소식 카드 유형
          enum:
          - gallery
          - share
        share:
          $ref: '#/components/schemas/ShareType'
        media:
          $ref: '#/components/schemas/MediaType'
        buttons:
          $ref: '#/components/schemas/Buttons'
    inline_response_200_1_result:
      type: object
      properties:
        feedId:
          type: string
          description: 소식ID
    inline_response_200_3_result:
      properties:
        feedId:
          maximum: 10
          type: string
          description: 소식 ID
  responses:
    Fail:
      description: 처리 실패
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/inline_response_default'
  parameters:
    BrandKey:
      name: X-RCS-Brandkey
      in: header
      description: |
        RCS Biz Center에서 브랜드 등록 시 자동 생성되는 Key 입니다.
        대행사가 브랜드 소식 API 연동 시 Header에 반드시 설정하여야 합니다.
      required: false
      style: simple
      explode: false
      schema:
        maximum: 36
        type: string
    BrandId:
      name: brandId
      in: path
      description: |
        브랜드 내 정보 접근시 사용되는 브랜드ID Path Parameter 입니다.
      required: true
      style: simple
      explode: false
      schema:
        type: string
    FeedId:
      name: feedId
      in: path
      description: |
        브랜드 소식 내 개별 소식 ID 입니다.
      required: true
      style: simple
      explode: false
      schema:
        type: string
    FileId:
      name: fileId
      in: path
      description: |
        브랜드 소식에 등록한 파일 ID Path Parameter 입니다.
      required: true
      style: simple
      explode: false
      schema:
        type: string
    FeedType:
      name: type
      in: query
      description: 소식 유형을 지정합니다.
      required: false
      style: form
      explode: true
      schema:
        type: string
        enum:
        - gallery
        - share
        - slide
    Offset:
      name: offset
      in: query
      description: "시작 offset 번호(default: 0) 입니다."
      required: false
      style: form
      explode: true
      schema:
        type: integer
    Limit:
      name: limit
      in: query
      description: "조회 최대 건수(default: 10, maximum: 100) 입니다."
      required: false
      style: form
      explode: true
      schema:
        type: integer
    StartDate:
      name: startDate
      in: query
      description: |
        검색 시작일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 부터  30일 이전 일자 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
      required: false
      style: form
      explode: true
      schema:
        maximum: 8
        type: string
    EndDate:
      name: endDate
      in: query
      description: |
        검색 종료일자(YYYYMMDD)를 지정합니다. 미지정 시 현재일 기준 전일로 기본 설정되며, 검색 시작일과 종료일의 범위는 최대 1개월입니다. 검색 가능 일자 범위는 전일 기준 1년 이내 입니다.
      required: false
      style: form
      explode: true
      schema:
        maximum: 8
        type: string
  securitySchemes:
    jwtAuth:
      type: http
      description: |
        인증방식은 JWT인증을 사용합니다. 토큰의 갱신은 없으며 토큰 만료 시 항상 재발급 받아야 합니다.
      scheme: bearer
      bearerFormat: JWT
